﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Students

{
    class Student
    {
        public string Name { get; set; }
      
        public int Age { get; set; }

        public int Height { get; set; }

        public string Hobby { get; set; }
    }
}
