﻿using System;

namespace Students
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.Name = "陈跃";
            student.Age = 19;
            student.Height = 175;
            student.Hobby = "打篮球";

            Console.WriteLine("姓名："+student.Name);
            Console.WriteLine("年龄："+student.Age);
            Console.WriteLine("身高："+student.Height);
            Console.WriteLine("爱好："+student.Hobby);
        }
    }
}
