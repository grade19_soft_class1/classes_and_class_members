﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Students.Class1;

namespace Students
{     
    
    class Program
    {
        static void Main(string[] args)
        {
            Class1 student = new Class1 ();
            student.Name = "黄鹏志";
            student.Age = "20";

            student.teacher = new Teacher()
            {
                Age = "不知道他多少",
                Name = "老胡",
                Sex = "男"
            };

            Console.WriteLine("我是{0},今年{1}岁！", student.Name, student.Age);

            Console.WriteLine("我有一个老师叫{0},讲真我{1}岁了！", student.teacher.Name, student.teacher.Age);

            student.Seat();

            Console.ReadKey();



        }
   
        
    }
}
