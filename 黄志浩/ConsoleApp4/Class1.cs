﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Class1
    {

        /// <summary>
        /// 名字
        /// </summary>
        public String name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int age { get; set; }

        /// <summary>
        /// 班级
        /// </summary>
        public String Class { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        public String major { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public String Studentid { get; set; }

        public void Studentcw()
        {

            Console.WriteLine("姓名：" + name);
            Console.WriteLine("年龄：" + age);
            Console.WriteLine("班级" + Class);
            Console.WriteLine("学号：" + Studentid);
            Console.WriteLine("专业：" + major);
        }
    }
}
