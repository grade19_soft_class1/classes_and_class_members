﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Student
{
    class Student
    {
        //名字
        public string Name { get; set; }
        //学号
        public int Id { get;set; }
        //年龄
        public int Age { get; set; }
        //班级
        public string Class { get; set; }
        //学校
        public string School { get; set; }
    }
}
