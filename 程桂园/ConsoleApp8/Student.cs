﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Student
    {
        public string num;
        public string name;
        public string sex;
        public int age;

        public void SayHi()
        {
            Console.WriteLine("大家好，我的学号是{0}，姓名是{1}，性别是{2}，年龄是{3}",
                this.num, this.name, this.sex, this.age);
        }
    }
}
