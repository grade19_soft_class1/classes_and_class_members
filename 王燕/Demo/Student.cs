﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Demo
{ 
    //在本项目内可访问
    internal class Student
    {
        public string Name { get; set; }
        public readonly int  Id;  //只读字段
        private int Year = 2020;   //私有字段
        public int Age;

    public Student()    //构造方法
        {
            this.Id = 19220101;
            Age = Year - 2001;
        }
    }
}
