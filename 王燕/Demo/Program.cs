﻿using System;

namespace Demo
{

    class Program
    {

        static void Main(string[] args)
        {


            var stu = new Student
            {
                Name = "王燕"
            };
            Console.WriteLine(stu.Name);
            Console.WriteLine(stu.Id);
            Console.WriteLine(stu.Age);

            //密封类实例化
            Teachers ts = new Teachers();
            ts.teacher = "胡哥";
            Console.WriteLine(ts.teacher);

            //静态可直接调用
            Class.ClassId = "软件一班";
            Console.WriteLine(Class.ClassId);


        }
    }
    //密封类
    sealed class Teachers
    {
        public string teacher { get; set; }
    }

}
