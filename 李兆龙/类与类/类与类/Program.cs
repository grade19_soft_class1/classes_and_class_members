using System;


namespace 类与类
{
    class Program
    {
        static void Main(string[] args)
        {
           
        }
        //abstract 抽象类 无法被实例化（无法new一个对象），只能被继承↓
       abstract class Id
        {
            public abstract  void Number();
        }

        class Num : Id   //继承抽象类
        {
            public override void Number()   //override 重写
            {
                Console.WriteLine("My Number is:1050621252");
            }
        }

        //sealed 封闭类 无法被继承,只能实例化↓

        sealed class Name
        {
            public String MyName = "Bruse Li";  //只读字段
        }

        public class Time
        {
            int A = 2020;
            public Time(int Year)  //构造函数
            {
                Age = A - Year;
            }
            public int Age { get; set; }
        }

       // static 静态类 不需要也无法被赋值↓

        static class School
        {
                String Name = "MD";
        }
}
