﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student("阿尔托莉雅",20,'女');
            student.Speak();
            Teacher.Name = "胡哥";
            Teacher.Age = 40;
            Console.WriteLine("我是你们的老师{0}，我今年{1}，我是{2}生",Teacher.Name,Teacher.Age,Teacher.Sex);
            Console.ReadKey();
        }
    }
}
