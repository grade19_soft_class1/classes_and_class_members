﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
  static class Teacher
    {
        private static string _name;
        public static  string Name
        {
             get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
           
        }
        private static int _age; 
        internal static int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }
        readonly static char _sex = '女';
        public static char Sex
        {
            get { return _sex; }
        }
      
    }
}
