﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Students students = new Students();
            students.Name = "熊耀扬";
            Console.WriteLine("姓名："+students.Name);
           

            Class @class = new Class("软件一班");
            Console.WriteLine("班级："+@class.Name);
 

            Teachers tes = new Teachers(); 
            tes.TeacherName(); 

            Students.Age = 19;
            Console.WriteLine("年龄："+ Students.Age);

            School sl = new School();
            Console.WriteLine( sl.SchoolName); 

            Sex sex = new Sex();
            sex.sex = "男";
            Console.WriteLine("性别：" + sex.sex);

    }
    }
    abstract class Teacher
    {
        public abstract void TeacherName();
    }

   class Teachers : Teacher 
    {  
        public override void TeacherName() // 
        {
            Console.WriteLine("教师："+"胡哥");
        }
    }
}


