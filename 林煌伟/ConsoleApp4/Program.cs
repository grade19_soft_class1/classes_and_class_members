﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassId.School = "闽大";
            ClassId c1 = new ClassId();            
            c1.C1 = "软件一班";
            Console.WriteLine(ClassId.School+"\t"+c1.C1);
            c1.TearchName = "胡老师";
            Console.WriteLine("我的老师是"+c1.TearchName );

            School sch = new School();
            {
                sch.Name();
            }

            StudentId si = new StudentId();
            Console.WriteLine("学号是"+si.StudentID);

            Console.WriteLine("请输入你的生日");
            int NowYear = int.Parse(Console.ReadLine());
            Age age = new Age(NowYear);
            Console.WriteLine("你今年"+age.Year+"岁");

            Work.HoneWork = "没有作业";
            Console.WriteLine(Work.HoneWork);
        }
        
    }

    //继承类
    abstract class Student
    {
        public abstract void Name();
    }

    class School : Student
    {
        public override void Name()//override重写
        {
            Console.WriteLine("My name is apply");
        }
    }

    class Age
    {
        //不公开
        private int now =2020;
               public  Age(int time)
        {
            Year = now - time;
        }
         public int Year;
        
    }
    
    static class Work
    {
        public static string HoneWork;
        
    }


}
