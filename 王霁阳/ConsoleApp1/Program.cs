﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.StudentAge = 18;
            student.StudentClass = "软件一班";
            student.StudentScore = 89;
            Console.WriteLine("学号 " + Student.StudentNumber);
            Console.WriteLine("名字 " + Student.StudentName);
            Console.WriteLine("班级 " + student.StudentClass);
            Console.WriteLine("年龄 " + student.StudentAge);
            Console.WriteLine("性别 " + Student.StudentSex);
            Console.WriteLine("成绩 " + student.StudentScore);
            Console.Read();
        }
    }
}
