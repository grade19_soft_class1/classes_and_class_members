﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {
        internal static string StudentNumber = "1522020519";//学号

        public static string StudentName = "王霁阳";

        public int StudentAge { get; set; }//年龄

        public string StudentClass { get; set; }//班级

        public int StudentScore { get; set; }//成绩

        internal static string StudentSex = "男";//性别
    }
}
