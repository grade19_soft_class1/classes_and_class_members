﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagement
{

    public class UserIndex
    {
        public UserIndex(string username, char usersex,int userage,long userid,string useraddress, string userwork,string userworkaddress)
        {
            this.UserName = username;
            this.UserSex = usersex;
            this.UserAge = userage;
            this.UserId = userid;
            this.UserAddress = useraddress;
            this.UserWork = userwork;
            this.UserWorkAddress = userworkaddress;
        }

        public string UserName { get; set; }//用户姓名

        public char UserSex { get; set; }//用户性别

        public int UserAge { get; set; }//用户年龄

        public long UserId { get; set; }//用户身份证号码

        public string UserAddress { get; set; }//用户居住地址

        public string UserWork { get; set; }//用户职业

        public string UserWorkAddress { get; set; }//用户工作或学习地点}

        public void PrintMsg()
    {
        Console.WriteLine("\n姓名：{0} \n性别：{1} \n年龄：{2} \n身份证号码：{3} \n居住地：{4} \n职业：{5} \n工作或学习地点：{6}", this.UserName, this.UserSex, this.UserAge, this.UserId, this.UserAddress, this.UserWork, this.UserWorkAddress);
    }
    }
    
    class UserPrivacyIndex
    {
        public int Money { get; private set; }
        public int Value { get; private set; }
        }
    }


