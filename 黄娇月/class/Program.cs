﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @class
{
    class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Age { get; set; }
        public string Number { get; set; }
        public string Major { get; set; }
        public string Performance { get; set; }
    }
    class Member
    {
        static void Main(string[] args)
        {
            Student student1 = new Student();
            Student student2 = new Student();

            //student1 情况
            student1.Name = "fff";
            student1.Class = "软1";
            student1.Age = "20";
            student1.Number = "1922010112";
            student1.Major = "软件技术";
            student1.Performance = "70";

            //student2 情况
            student2.Name = "aaa";
            student2.Class = "软1";
            student2.Age = "20";
            student2.Number = "2344567685";
            student2.Major = "软件技术";
            student2.Performance = "80";
        }
    }
}
