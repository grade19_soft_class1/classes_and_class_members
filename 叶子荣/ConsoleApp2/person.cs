﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class person
    {
        private int _age;
        public int Age
        {
            get { return Age; }
            set {
                if(value < 0 || value > 100)
                {
                    value = 0;
                }
                
                _age = value; }
        }



        private string _name;
         public  string Name  //属性，用来保护字段的赋值非法性
        {
            get { return _name; }
            set { _name = value; }
        }


        private string _sex;
        public string Sex
        {
            get { 
                if(_sex != "男" && _sex != "女")
                {
                    _sex = "男";
                }
                
                return _sex; }
            set {_sex = value; }
        }

        public void behavior()
        {
            Console.WriteLine("我今年{0}岁，我叫{1}，我是{2}生，",this.Age,this.Name,this.Sex);
        }
    }
}
