﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 静态非静态
{
     public static class Studen
        //静态类中只允许出现静态成员
        //如果经常使用一个类，可以考虑将这个类写成静态类，静态类在整个项目中资源共享
    {
        private static string _name;

        public static string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public static  void M1()
        {
            Console.WriteLine("Hello World");
        }

       //  public int Age;  error  静态类中不允许出现实例成员

    }
}
