﻿using System;

namespace 静态非静态
{
    class Program
    {
        static void Main(string[] args)
        {
            //调用实例成员
            person p = new person();
            p.M1();
            person.M2();   //调用静态成员时需要使用 类名.静态成员名。
            Console.ReadKey();
        }
    }
}
