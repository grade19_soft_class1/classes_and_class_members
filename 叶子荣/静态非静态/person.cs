﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace 静态非静态
{
    public  class person
    {

        //静态成员必须使用类去调用，实例成员使用对象去调用
        //静态函数中只允许访问静态成员不允许访问实例成员
        //实例函数中既可以使用静态成员也可以使用实例成员
        private static string _name;
        public static string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private static int _age;
        public static int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private  string  _sex;
        public  string Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }

        

        public void M1()
        {   
            
            Console.WriteLine("我是非静态成员");
        }

        public static void M2()
        {   
            
            Console.WriteLine("我是静态成员");
        }
    }
}
