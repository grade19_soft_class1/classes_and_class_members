﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Student
    {
        public string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int age;
        internal int Age
        {
            get { return age; }
            set
            {
                if (value < 0 || value > 100)
                {
                    value = 0;
                }
                age = value;
            }
        }

        protected readonly char sex;
        private static int id;
        private const string major = "软件";


        public void skill()
        {
            Console.WriteLine("姓名{0},年龄{1}，我会"+major , this.name, this.age);
        }
    }
}
