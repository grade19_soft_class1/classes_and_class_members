﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("请输入你的出生年份");
            int Age = int.Parse(Console.ReadLine());
            Year yy = new Year(Age);            //强行初始化
            Console.WriteLine("我的年龄为：" + yy.Age);


            School.SchoolName = "闽西职业技术学院";  //静态类不需要实例化 直接赋值
            School sch = new School();
            {
                sch.Name();
            };
            Console.WriteLine("学校："+School.SchoolName);

            People Pp = new People();  
            Pp.Sex();     
            Pp.ID();

            Classid cI = new Classid();  //实例化班级
            cI.cI = "软件一班";
            Console.WriteLine(cI.cI);  

            Teacher tea = new Teacher();
            Console.WriteLine("我的上课老师："+tea.TeacherName);


        }
    }
    public class Classid    //公共类  
    {
        public string cI { get; set; }    
    }
    abstract class Student   //抽象类 只能被继承
    {
        public abstract void Name();
    }
    class School : Student //继承抽象类
    { 
        public override void Name()    // override重写
        {
            Console.WriteLine("我的名字是：景衡光"); //重写内容
        }
       
         public static string SchoolName;   //公共 静态 不需要实例化
    }
    sealed class Teacher  // 密封类 只能实例化，不能被继承
    {
        public readonly string TeacherName = "老胡";   //只读字段
    }
    class Year
    {
        private int A = 2020;    //私有字段
        public Year(int age)  //构造函数
        {
            Age = A - age;
        }
        public int Age { get; set; }
    }

    /// <summary>
    /// 接口函数成员不能有主体  只能在派生类中去实现
    /// </summary>
    interface Animal    //接口
    {
        void Sex();     //性别
    }
    interface Anima2    //接口
    {
        void ID();     //学号
    }
    class People : Animal, Anima2    //定义类继承接口 可以继承多个接口 
    {
        public void ID()
        {
            Console.WriteLine("学号："+1922010118);
        }

        public void Sex()
        {
            Console.WriteLine("性别：男");
        }
    }
}
