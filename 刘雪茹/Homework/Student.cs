﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    public  class Student 
    {                                
        public string name;
         public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int age;
         internal int Age
        {
            get { return age; }
            set {
                if(value <0 || value >100)
                {
                    value = 0;
                }
                age = value; }
        }
         
        protected readonly char sex;
        private static int id;
        private const string major = "软件";

  
        public void Skill()
        {
            Console.WriteLine("我叫{0},我今年{1}，我会吃喝玩乐",this.name ,this.age );
        }
    }

    

    
}
